//
//  MapVC.m
//  Saza
//
//  Created by MAPPS MAC on 05/05/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "MapVC.h"
#import "AppDelegate.h"
@interface MapVC (){
    AppDelegate *delegate;
}
- (IBAction)btnBackAction:(UIButton *)sender;
- (IBAction)btnNextAction:(UIButton *)sender;
- (IBAction)btnHomeAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(delegate.isEnglish==YES){
        [self.btnBack setTitle:[delegate.dictEnglish valueForKey:@"back"] forState:UIControlStateNormal];
        [self.btnHome setTitle:[delegate.dictEnglish valueForKey:@"home"] forState:UIControlStateNormal];
        [self.btnNext setTitle:[delegate.dictEnglish valueForKey:@"next"] forState:UIControlStateNormal];
    }else{
        [self.btnBack setTitle:[delegate.dictArabian valueForKey:@"back"] forState:UIControlStateNormal];
        [self.btnHome setTitle:[delegate.dictArabian valueForKey:@"home"] forState:UIControlStateNormal];
        [self.btnNext setTitle:[delegate.dictArabian valueForKey:@"next"] forState:UIControlStateNormal];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.myMap setRegion:[self.myMap regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    [self.myMap addAnnotation:point];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNextAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueNewLoc" sender:self];
}

- (IBAction)btnHomeAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
