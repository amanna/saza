//
//  HomeVC.h
//  Saza
//
//  Created by MAPPS MAC on 27/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnEservice;
@property (weak, nonatomic) IBOutlet UIButton *btnNewFile;
@property (weak, nonatomic) IBOutlet UIButton *btnAmbulance;
@property (weak, nonatomic) IBOutlet UIButton *btnEnglish;
@property (weak, nonatomic) IBOutlet UIButton *btnArabian;
- (IBAction)btnEserviceAction:(id)sender;
- (IBAction)btnNewFileAction:(id)sender;
- (IBAction)btnAmbulanceAction:(id)sender;
- (IBAction)btnEnglishAction:(id)sender;

- (IBAction)btnArabianAction:(UIButton *)sender;
@end
