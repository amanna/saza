//
//  POAcvityView.h
//  Pop
//
//  Created by Praveen on 07/09/13.
//  Copyright (c) 2013 Praveen@Tulieservices.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface POAcvityView : UIView{

    UIView *parentView;
	NSString *title;
	NSString *message;
}
- (id)initWithTitle:(NSString *)title message:(NSString *)message;

- (void) showView;
- (void) hideView;

@end
