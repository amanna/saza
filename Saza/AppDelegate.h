//
//  AppDelegate.h
//  Saza
//
//  Created by MAPPS MAC on 26/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic)BOOL isEnglish;
@property(nonatomic)NSMutableDictionary *dictEnglish;
@property(nonatomic)NSMutableDictionary *dictArabian;
- (void)composeEnglish;
- (void)composeArabian;
@end

