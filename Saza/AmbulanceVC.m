//
//  AmbulanceVC.m
//  Saza
//
//  Created by MAPPS MAC on 05/05/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AmbulanceVC.h"
#import "AppDelegate.h"
#import "LocationVC.h"
#import <CoreLocation/CoreLocation.h>
@interface AmbulanceVC ()<CLLocationManagerDelegate>{
    CLLocationManager *locationManager;

    AppDelegate *delegate;
    NSString *strLatt;
    NSString *strLongi;
}
- (IBAction)btnCurrentLocaction:(UIButton *)sender;
- (IBAction)chooseAnotherLoc:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrLoc;
@property (weak, nonatomic) IBOutlet UIButton *btnAnotherLoc;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btnBackAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)btnHomeAction:(UIButton *)sender;

@property(nonatomic)BOOL isFlag;
@end

@implementation AmbulanceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLLocationAccuracyBest;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [locationManager requestAlwaysAuthorization]; //Note this one
    [locationManager startUpdatingLocation];

     delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(delegate.isEnglish==YES){
        [self.btnBack setTitle:[delegate.dictEnglish valueForKey:@"back"] forState:UIControlStateNormal];
        [self.btnHome setTitle:[delegate.dictEnglish valueForKey:@"home"] forState:UIControlStateNormal];
        [self.btnCurrLoc setTitle:[delegate.dictEnglish valueForKey:@"current_location"] forState:UIControlStateNormal];
        [self.btnAnotherLoc setTitle:[delegate.dictEnglish valueForKey:@"choose_location"] forState:UIControlStateNormal];
        
    }else{
        [self.btnBack setTitle:[delegate.dictArabian valueForKey:@"back"] forState:UIControlStateNormal];
        [self.btnHome setTitle:[delegate.dictArabian valueForKey:@"home"] forState:UIControlStateNormal];
        [self.btnCurrLoc setTitle:[delegate.dictArabian valueForKey:@"current_location"] forState:UIControlStateNormal];
        [self.btnAnotherLoc setTitle:[delegate.dictArabian valueForKey:@"choose_location"] forState:UIControlStateNormal];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCurrentLocaction:(UIButton *)sender {
    if(delegate.isEnglish==YES){
        NSString *strCancel = [NSString stringWithFormat:@"%@",[delegate.dictEnglish valueForKey:@"cancel"]];
        NSString *strSend = [NSString stringWithFormat:@"%@",[delegate.dictEnglish valueForKey:@"send"]];
        NSString *strMsg = [NSString stringWithFormat:@"%@ %@ %@",];
    }else{
        NSString *strCancel = [NSString stringWithFormat:@"%@",[delegate.dictArabian valueForKey:@"cancel"]];
        NSString *strSend = [NSString stringWithFormat:@"%@",[delegate.dictArabian valueForKey:@"send"]];
        NSString *strMsg = [NSString stringWithFormat:@"%@ %@ %@",];
    }
   
    
    UIAlertView *myAlert = [[UIAlertView alloc]
                            initWithTitle:@""
                            message:@"Message"
                            delegate:self
                            cancelButtonTitle:@"Cancel"
                            otherButtonTitles:@"Ok",nil];
    [myAlert show];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"%f ------- %f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    strLatt = [[NSNumber numberWithFloat:newLocation.coordinate.latitude] stringValue];
    
    strLongi = [[NSNumber numberWithFloat:newLocation.coordinate.longitude] stringValue];
    [manager stopUpdatingLocation];
    
    if(delegate.isEnglish==YES){
        self.strWebUrl = [NSString stringWithFormat:@"http://test.sajaclinics.com/Patient/AmbulanceRequests?lat=%@&lag=%@&lang=en",strLatt,strLongi];
    }else{
        self.strWebUrl = [NSString stringWithFormat:@"http://test.sajaclinics.com/Patient/AmbulanceRequests?lat=%@&lag=%@&lang=ar",strLatt,strLongi];
    }
    
    //http://test.sajaclinics.com/Patient/AmbulanceRequests?lat=22.4450423&lag=88.2971343&lang=en
    //http://test.sajaclinics.com/Patient/AmbulanceRequests?lat=22.4450423&lag=88.2971343&lang=ar
    if(self.isFlag==true){
        self.isFlag = false;
        //        [WebServiceManager saveMyLatLongitude:strLatt andLoni:strLongi onCompletion:^(id object, NSError *error) {
        //            if(object){
        //                self.isFlag = true;
        //                NSLog(@"save location");
        //            }else{
        //                NSLog(@"not saving location");
        //            }
        //        }];
    }
    
    
    
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueLoc"]) {
        LocationVC* loc = [segue destinationViewController];
        loc.strWebUrl = self.strWebUrl;
       
    }
}
- (IBAction)chooseAnotherLoc:(id)sender {
    [self performSegueWithIdentifier:@"segueMap" sender:self];
    
}

- (IBAction)btnBackAction:(UIButton *)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnHomeAction:(UIButton *)sender {
     [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
