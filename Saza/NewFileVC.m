//
//  NewFileVC.m
//  Saza
//
//  Created by Aditi on 05/05/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "NewFileVC.h"
#import "AppDelegate.h"
#import "POAcvityView.h"
@interface NewFileVC (){
    POAcvityView *activity;
    AppDelegate *delegate;

}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@end

@implementation NewFileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [activity showView];
    NSString *strEservice;
    if(delegate.isEnglish==YES){
        strEservice = @"http://test.sajaclinics.com/Patient/PatientNew?lang=en";
         [self.btnHome setTitle:[delegate.dictEnglish valueForKey:@"home"] forState:UIControlStateNormal];
    }else{
        strEservice = @"http://test.sajaclinics.com/Patient/PatientNew?lang=ar";
         [self.btnHome setTitle:[delegate.dictArabian valueForKey:@"home"] forState:UIControlStateNormal];
    }
    NSURL *url = [NSURL URLWithString:strEservice];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebview loadRequest:request];

    // Do any additional setup after loading the view.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity hideView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
