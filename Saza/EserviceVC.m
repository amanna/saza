//
//  EserviceVC.m
//  Saza
//
//  Created by MAPPS MAC on 27/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "EserviceVC.h"
#import "POAcvityView.h"
#import "AppDelegate.h"
@interface EserviceVC (){
    POAcvityView *activity;
    AppDelegate *delegate;
}
- (IBAction)btnHomeAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@end

@implementation EserviceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [activity showView];
    NSString *strEservice;
    if(delegate.isEnglish==YES){
        strEservice = @"http://test.sajaclinics.com/Patient/PatientLogin?lang=en";
        [self.btnHome setTitle:[delegate.dictEnglish valueForKey:@"home"] forState:UIControlStateNormal];
    }else{
         strEservice = @"http://test.sajaclinics.com/Patient/PatientLogin?lang=ar";
        [self.btnHome setTitle:[delegate.dictArabian valueForKey:@"home"] forState:UIControlStateNormal];
    }
    NSURL *url = [NSURL URLWithString:strEservice];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebview loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity hideView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnHomeAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
