//
//  LocationVC.m
//  Saza
//
//  Created by MAPPS MAC on 28/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "LocationVC.h"
#import "POAcvityView.h"
#import "AppDelegate.h"
@interface LocationVC (){
    POAcvityView *activity;
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@end

@implementation LocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [activity showView];
    NSURL *url = [NSURL URLWithString:self.strWebUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebview loadRequest:request];
  // Do any additional setup after loading the view.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity hideView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
