//
//  AppDelegate.m
//  Saza
//
//  Created by MAPPS MAC on 26/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
//    {
//        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
//        view.backgroundColor=[UIColor colorWithRed:37/255 green:45/255 blue:149/255 alpha:1.0];
//        [self.window.rootViewController.view addSubview:view];
//    }

    self.dictArabian = [[NSMutableDictionary alloc]init];
    self.dictEnglish = [[NSMutableDictionary alloc]init];
    [self composeEnglish];
    [self composeArabian];
    
    self.isEnglish= YES;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)composeEnglish{
    [self.dictEnglish setObject:@"Saja Clinics" forKey:@"app_name"];
    [self.dictEnglish setObject:@"Map" forKey:@"title_activity_maps"];
    [self.dictEnglish setObject:@"E - Service" forKey:@"e_services"];
    [self.dictEnglish setObject:@"Ambulance Request" forKey:@"ambulance_request"];
    [self.dictEnglish setObject:@"Use Your Current Location" forKey:@"current_location"];
    [self.dictEnglish setObject:@"Choose a Location" forKey:@"choose_location"];
    [self.dictEnglish setObject:@"Please wait .." forKey:@"please_wait"];
    [self.dictEnglish setObject:@"Hold long on the marker, Drag then Drop." forKey:@"drag_note"];
    [self.dictEnglish setObject:@"Back" forKey:@"back"];
    [self.dictEnglish setObject:@"Next" forKey:@"next"];
    [self.dictEnglish setObject:@"Home" forKey:@"home"];
    [self.dictEnglish setObject:@"العربية" forKey:@"arabic"];
    [self.dictEnglish setObject:@"English" forKey:@"english"];
    [self.dictEnglish setObject:@"New File" forKey:@"new_file"];
    [self.dictEnglish setObject:@"No Internet Connection" forKey:@"no_internet_connection"];
    [self.dictEnglish setObject:@"Hold Long Then Drag" forKey:@"hold_long_drag"];
    [self.dictEnglish setObject:@"Drop at Location" forKey:@"Drop_at_Location"];
    [self.dictEnglish setObject:@"Please drop the marker to the required location" forKey:@"Please_drop_marker"];
    [self.dictEnglish setObject:@"Got Location" forKey:@"Got_Location"];
    [self.dictEnglish setObject:@"Click Next" forKey:@"Click_next"];
    [self.dictEnglish setObject:@"Enable GPS/Location from you Phone Settings" forKey:@"Enable_gps"];
    [self.dictEnglish setObject:@"Send" forKey:@"send"];
    [self.dictEnglish setObject:@"Cancel" forKey:@"cancel"];
    [self.dictEnglish setObject:@"Searching.." forKey:@"searching"];
}
- (void)composeArabian{
    [self.dictArabian setObject:@"Saja Clinics" forKey:@"app_name"];
    [self.dictArabian setObject:@"الخريطه" forKey:@"title_activity_maps"];
    [self.dictArabian setObject:@"الخدمات الإلكترونية" forKey:@"e_services"];
    
    [self.dictArabian setObject:@"طلب سيارة إسعاف" forKey:@"ambulance_request"];
    [self.dictArabian setObject:@"إستخدم موقع الحالي" forKey:@"current_location"];
    [self.dictArabian setObject:@"اختيار موقع آخر" forKey:@"choose_location"];
    [self.dictArabian setObject:@"أرجو الإنتظار" forKey:@"please_wait"];
    [self.dictArabian setObject:@"إستمر في الضغط على الموقع المطلوب ثم أفلت" forKey:@"drag_note"];
    [self.dictArabian setObject:@"السابق" forKey:@"back"];
    [self.dictArabian setObject:@"التالى" forKey:@"next"];
    [self.dictArabian setObject:@"الصفحة الرئيسية" forKey:@"home"];
    [self.dictArabian setObject:@"العربية" forKey:@"arabic"];
    [self.dictArabian setObject:@"English" forKey:@"english"];
    [self.dictArabian setObject:@"ملف جديد" forKey:@"new_file"];
    [self.dictArabian setObject:@"لا يوجد إتصال بالإنترنت" forKey:@"no_internet_connection"];
    [self.dictArabian setObject:@"إستمر في الضغط ثم أفلت" forKey:@"hold_long_drag"];
    [self.dictArabian setObject:@"إسقط العلامة في الموقع المطلوب" forKey:@"Drop_at_Location"];
    [self.dictArabian setObject:@"يرجى إسقاط العلامة إلى الموقع المطلوب" forKey:@"Please_drop_marker"];
    [self.dictArabian setObject:@"تم تحديد الموقع" forKey:@"Got_Location"];
    [self.dictArabian setObject:@"انقر التالي" forKey:@"Click_next"];
    [self.dictArabian setObject:@"يجب تفعيل نظام تحديد المواقع" forKey:@"Enable_gps"];
    [self.dictArabian setObject:@"إرسال" forKey:@"send"];
    [self.dictArabian setObject:@"إلغاء" forKey:@"cancel"];
    [self.dictArabian setObject:@"البحث" forKey:@"searching"];
    
}
@end
