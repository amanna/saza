//
//  HomeVC.m
//  Saza
//
//  Created by MAPPS MAC on 27/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "HomeVC.h"
#import "AppDelegate.h"
@interface HomeVC (){
     AppDelegate *delegate;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(delegate.isEnglish==YES){
        [self.btnEservice setTitle:[delegate.dictEnglish valueForKey:@"e_services"] forState:UIControlStateNormal];
        [self.btnNewFile setTitle:[delegate.dictEnglish valueForKey:@"new_file"] forState:UIControlStateNormal];
        [self.btnAmbulance setTitle:[delegate.dictEnglish valueForKey:@"ambulance_request"] forState:UIControlStateNormal];
        
        
    }else{
        [self.btnEservice setTitle:[delegate.dictArabian valueForKey:@"e_services"] forState:UIControlStateNormal];
        [self.btnNewFile setTitle:[delegate.dictArabian valueForKey:@"new_file"] forState:UIControlStateNormal];
        [self.btnAmbulance setTitle:[delegate.dictArabian valueForKey:@"ambulance_request"] forState:UIControlStateNormal];
      

    }
    [self.btnArabian setTitle:[delegate.dictEnglish valueForKey:@"arabic"] forState:UIControlStateNormal];
    [self.btnEnglish setTitle:[delegate.dictEnglish valueForKey:@"english"] forState:UIControlStateNormal];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnEserviceAction:(id)sender {
    [self performSegueWithIdentifier:@"segueEservice" sender:self];
    
}

- (IBAction)btnNewFileAction:(id)sender {
     [self performSegueWithIdentifier:@"segueNewFile" sender:self];
}

- (IBAction)btnAmbulanceAction:(id)sender {
     [self performSegueWithIdentifier:@"segueAmbulance" sender:self];
}

- (IBAction)btnEnglishAction:(id)sender {
    delegate.isEnglish = YES;
    [self viewDidLoad];
    [self viewWillAppear:YES];
}

- (IBAction)btnArabianAction:(UIButton *)sender {
    delegate.isEnglish= NO;
    [self viewDidLoad];
     [self viewWillAppear:YES];
}
@end
